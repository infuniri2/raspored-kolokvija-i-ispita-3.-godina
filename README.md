Raspored kolokvija i ispita - 3. godina, jednompredmetna informatika
====================================================================

Autori:

- Kristijan Lenković
- Edvin Močibob


Sadržaj repozitrija
-------------------

- Popis datuma kolokvija i ispita
- InfUniRi2 kalendar s upisanim kolokvijima i ispitima
- Ostali dokumenti namjenjeni za predstavnike 3. godine